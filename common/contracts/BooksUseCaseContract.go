package contracts

import (
	"gotest/common/models"
)

// BooksUseCaseContract is the contract will be implemented for concrete use case of books
type BooksUseCaseContract interface {
	Create(isbn string, title string, author string, category string) error
	GetOne(isbn string) (*models.Book, error)
	GetByCategory(category string) (*[]models.Book, error)
	EraseOne(isbn string) error
}
