package controllers

import (
	"context"
	"encoding/json"
	"gotest/common/contracts"

	"github.com/aws/aws-lambda-go/events"
)

// BooksFindOneController is a struct to handle incoming request
type BooksFindOneController struct {
	UseCase contracts.BooksUseCaseContract
}

// NewBooksFindOneController is a BooksFindOneController constructor
func NewBooksFindOneController(UseCase contracts.BooksUseCaseContract) *BooksFindOneController {
	return &BooksFindOneController{
		UseCase: UseCase,
	}
}

// Handler is a method contract to implement a aws request and response handler
func (Controller BooksFindOneController) Handler(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	bk, err := Controller.UseCase.GetOne(request.PathParameters["isbn"])
	if err != nil {
		return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 200}, nil
	}

	body, err := json.Marshal(bk)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: "Error: converting book to json", StatusCode: 200}, nil
	}

	return events.APIGatewayProxyResponse{Body: string(body), StatusCode: 200}, nil
}
