package models

// Book is a struct that represent a model in dynamodb
type Book struct {
	ISBN     string `json:"isbn"`
	Title    string `json:"title"`
	Author   string `json:"author"`
	Category string `json:"category"`
}

// NewBook is a constructor for Book structure
func NewBook(ISBN string, Title string, Author string, Category string) *Book {
	return &Book{
		ISBN:     ISBN,
		Title:    Title,
		Author:   Author,
		Category: Category,
	}
}
