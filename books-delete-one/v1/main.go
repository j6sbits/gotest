package main

import (
	"gotest/common"
	"gotest/common/controllers"
	"gotest/common/repositories"
	"gotest/common/usecases"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

func main() {
	Client := dynamodb.New(session.New(), aws.NewConfig().WithRegion("us-east-1"))
	Repository := repositories.NewBooksDynamoRepository(Client)
	UseCase := usecases.NewBooksUseCase(Repository)
	Controller := controllers.NewBooksDeleteOneController(UseCase)

	Lambda := common.NewAwsLambda(Controller)
	Lambda.Start()
}
